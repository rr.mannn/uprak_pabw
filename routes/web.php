<?php

use App\Models\Produk;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\VideoController;
use App\Http\Controllers\ProdukController;
use App\Http\Controllers\ArtikelController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [HomeController::class, 'index']);
Route::get('/produk', [ProdukController::class, 'index']);
Route::get('/video', [VideoController::class, 'index']);
Route::get('/artikel', [ArtikelController::class, 'index']);
Route::get('/tentang-kami', [HomeController::class, 'tentangkami']);


Route::get('/login', [HomeController::class, 'login'])->name('login')->middleware('guest');
Route::post('/login', [HomeController::class, 'authenticate']);
Route::get('/signup', [HomeController::class, 'register'])->middleware('guest');
Route::post('/signup', [HomeController::class, 'store']);
Route::post('/logout', [HomeController::class, 'logout']);

Route::get('/dashboard', [HomeController::class, 'dashboard'])->middleware('auth');

Route::get('/dashboard/data-produk', [ProdukController::class, 'dataproduk'])->middleware('auth');
Route::get('/dashboard/tambah-data-produk', [ProdukController::class, 'tambahdataproduk'])->middleware('auth');

Route::get('/dashboard/data-artikel', [ArtikelController::class, 'dataartikel'])->middleware('auth');
Route::get('/dashboard/tambah-data-artikel', [ArtikelController::class, 'tambahdataartikel'])->middleware('auth');

Route::get('/dashboard/data-video', [VideoController::class, 'datavideo'])->middleware('auth');
Route::get('/dashboard/tambah-data-video', [VideoController::class, 'tambahdatavideo'])->middleware('auth');
