<?php

namespace App\Http\Controllers;

use App\Models\Video;
use Illuminate\Http\Request;

class VideoController extends Controller
{
    public function index()
    {
        return view('video', [
            'title' => 'All Posts',
            'posts' => Video::latest()->get()
        ]);
    }

    public function detail(Video $video)
    {
        return view('detail-video', [
            'title' => "Detail Video",
            'video' => $video
        ]);
    }

    public function datavideo()
    {
        return view('dashboard/video/data-video', [
            'title' => 'Data video',
            'video' => Video::latest()->get()
        ]);
    }

    public function tambahdatavideo()
    {
        return view('dashboard.video.tambah-data-video', [
            'title' => 'Tambah Data video',
            'video' => Video::latest()->get()
        ]);
    }
}
