<?php

namespace App\Http\Controllers;


use App\Models\User;
use App\Models\Artikel;
use App\Models\Kategori;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redis;
use \Cviebrock\EloquentSluggable\Services\SlugService;

class ArtikelController extends Controller
{

    public function index()
    {
        return view('artikel', [
            'title' => 'Artikel',
            'posts' => Artikel::latest()->get()
        ]);
    }


    public function dataartikel()
    {
        return view('dashboard/artikel/data-artikel', [
            'title' => 'Data artikel',
            'artikel' => Artikel::latest()->get(),
            'user' => User::all(),
            'kategori' => Kategori::all()
        ]);
    }

    public function tambahdataartikel()
    {
        return view('dashboard/artikel/tambah-data-artikel', [
            'title' => 'Tambah Data artikel',
            'kategori' => Kategori::all(),
            'artikel' => Artikel::latest()->get()
        ]);
    }

    public function createSlug(Request $request)
    {
        $slug = SlugService::createSlug(Post::class, 'slug', $request->nama);
        return response()->json(['slug' => $slug]);
    }

    public function store(Request $request)
    {
        $validatedData =  $request->validate([
            'nama' => 'required|max:255',
            'slug' => 'required|unique:artikels',
            'kategori_id' => 'required',
            'deskripsi' => 'required'
        ]);

        $validatedData['user_id'] = auth()->user()->id;
        $validatedData['foto'] = '/default';

        Artikel::create($validatedData);

        return redirect('/dashboard/data-artikel')->with('success', 'Data berhasil ditambahkan!');
    }

    public function delete($id)
    {
        // $data = Artikel::find($id);
        // $data->delete();

        DB::table('artikels')->where('id', $id)->delete();

        return redirect('/dashboard/data-artikel')->with('success', 'Artikel berhasil dihapus');
    }

    public function edit($id)
    {
        $artikel = DB::table('artikels')->where('id', $id)->first();
        $slug = $artikel->slug;
        $nama = $artikel->nama;
        $kategori = $artikel->kategori_id;
        $deskripsi = $artikel->deskripsi;
        $id = $artikel->id;
        return view('dashboard/artikel/edit', [
            'title' => 'Edit Data artikel',
            'judul_artikel' => $nama,
            'slug_artikel' => $slug,
            'kategori_artikel' => $kategori,
            'deskripsi_artikel' => $deskripsi,
            'id_artikel' => $id,
            'kategori' => Kategori::all(),
            'artikel' => Artikel::latest()->get()
        ]);
    }

    public function editdataartikel(Request $request, $id)
    {
        $validatedData =  $request->validate([
            'nama' => 'required|max:255',
            'slug' => 'required',
            'kategori_id' => 'required',
            'deskripsi' => 'required'
        ]);

        Artikel::where('id', $id)->update($validatedData);
        return redirect('/dashboard/data-artikel')->with('success', 'Artikel berhasil diubah');
    }
}
