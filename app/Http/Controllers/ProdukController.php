<?php

namespace App\Http\Controllers;

use App\Models\Produk;
use App\Models\Kategori;
use Illuminate\Http\Request;

class ProdukController extends Controller
{
    public function index()
    {
        return view('produk', [
            'title' => 'All Product',
            'posts' => Produk::latest()->get()
        ]);
    }

    public function dataproduk()
    {
        return view('dashboard/produk/dataproduk', [
            'title' => 'Data Produk',
            'produk' => Produk::latest()->get()
        ]);
    }

    public function tambahdataproduk()
    {
        return view('dashboard.produk.tambah-data-produk', [
            'title' => 'Tambah Data Produk',
            'produk' => Produk::latest()->get(),
            'kategori' => Kategori::all()
        ]);
    }
}
