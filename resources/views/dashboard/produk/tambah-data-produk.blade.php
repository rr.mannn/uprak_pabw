@extends('dashboard.layout.main')

@section('container')
<div class="container-fluid">
    <div class="row justify-content-center">
        <div class="col-12">
            <h2 class="page-title">Tambah Data Produk</h2>
            <p class="text-muted">Demo for form control styles, layout options, and custom components for
                creating a wide variety of forms.</p>
            <div class="card shadow mb-4">
                <div class="card-header">
                    <strong class="card-title">Form Produk</strong>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group mb-3">
                                <label for="simpleinput">Nama</label>
                                <input type="text" id="simpleinput" class="form-control">
                            </div>
                            <div class="form-group mb-3">
                                <label for="simpleinput">Berat</label>
                                <input type="text" id="simpleinput" class="form-control" placeholder="gram">
                            </div>
                            <div class="form-group mb-3">
                                <label for="simpleinput">Harga</label>
                                <input type="text" id="simpleinput" class="form-control" placeholder="Rp.">
                            </div>
                            <div class="form-group mb-3">
                                <label for="custom-select">Kategori</label>
                                <select class="custom-select" id="custom-select">
                                    <option selected>pilih</option>
                                    <option value="1">Kategori 1</option>
                                    <option value="2">Kategori 2</option>
                                    <option value="3">Kategori 3</option>
                                </select>
                            </div>
                            <div class="form-group mb-3">
                                <label for="example-textarea">Deskripsi</label>
                                <textarea class="form-control" id="example-textarea" rows="4"></textarea>
                            </div>
                            <button class="btn btn-primary" type="submit">Tambah Data</button>
                        </div> <!-- /.col -->
                        <div class="col-md-6">
                            <div class="card-header">
                                <strong>Foto</strong>
                            </div>
                            <div class="card-body">
                                <form action="/file-upload" class="dropzone bg-light rounded-lg"
                                    id="tinydash-dropzone">
                                    <div class="dz-message needsclick">
                                        <div class="circle circle-lg bg-primary">
                                            <i class="fe fe-upload fe-24 text-white"></i>
                                        </div>
                                        <h5 class="text-muted mt-4">Drop files here or click to upload</h5>
                                    </div>
                                </form>
                                <!-- Preview -->
                                <!-- <div class="dropzone-previews mt-3" id="file-previews"></div> -->
                                <!-- file preview template -->
                                <div class="d-none" id="uploadPreviewTemplate">
                                    <div class="card mt-1 mb-0 shadow-none border">
                                        <div class="p-2">
                                            <div class="row align-items-center">
                                                <div class="col-auto">
                                                    <img data-dz-thumbnail src="#"
                                                        class="avatar-sm rounded bg-light" alt="">
                                                </div>
                                                <div class="col pl-0">
                                                    <a href="javascript:void(0);"
                                                        class="text-muted font-weight-bold"
                                                        data-dz-name></a>
                                                    <p class="mb-0" data-dz-size></p>
                                                </div>
                                                <div class="col-auto">
                                                    <!-- Button -->
                                                    <a href="" class="btn btn-link btn-lg text-muted"
                                                        data-dz-remove>
                                                        <i class="dripicons-cross"></i>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div> <!-- / .card -->
            </div> <!-- .col-12 -->
        </div> <!-- .row -->
    </div> <!-- .container-fluid -->
</div>
@endsection