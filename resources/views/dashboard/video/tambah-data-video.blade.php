@extends('dashboard.layout.main')

@section('container')
<div class="container-fluid">
    <div class="row justify-content-center">
        <div class="col-12">
            <h2 class="page-title">Tambah Data Artikel</h2>
            <p class="text-muted">Menambahkan data artikel</p>
            <div class="card shadow mb-4">
                <div class="card-header">
                    <strong class="card-title">Form Artikel</strong>
                </div>
                <div class="card-body">
                    <form class="row" method="POST" action="/dashboard/tambah-data-artikel">
                        @csrf
                        <div class="col-md-12">
                            <div class="form-group mb-3">
                                <label for="simpleinput">Judul</label>
                                <input type="text" id="nama" name="nama" value="{{ old('nama') }}" class="form-control">
                                @error('nama')
                          <p class="gtn-error" style="color: red; font-size:small">{{ $message }}</p>
                        @enderror
                            </div>
                            <div class="form-group mb-3">
                                <label for="slug">Slug</label>
                                <input value="{{ old('slug') }}" type="slug" id="slug" name="slug"
                                    class="form-control">
                                    @error('slug')
                          <p class="gtn-error" style="color: red; font-size:small">{{ $message }}</p>
                        @enderror
                            </div>
                            <div class="form-group mb-3">
                                <label for="kategori_id">Kategori</label>
                                <select class="custom-select" id="kategori_id" name="kategori_id">
                                    <option >pilih</option>
                                    @foreach ($kategori as $kat)
                                        <option value="{{ $kat['id'] }}">{{ $kat['nama'] }}</option>
                                    @endforeach
                                </select>
                                @error('kategori_id')
                          <p class="gtn-error" style="color: red; font-size:small">{{ $message }}</p>
                        @enderror
                            </div>
                            <div class="form-group mb-3">
                                <label for="deskripsi">Deskripsi</label>
                                <textarea  value="{{ old('deskripsi') }}" id="deskripsi" class="form-control" name="deskripsi" rows="10" cols="50"></textarea>
                                @error('deskripsi')
                          <p class="gtn-error" style="color: red; font-size:small">{{ $message }}</p>
                        @enderror
                            </div>
                            <button class="btn btn-primary" type="submit">Tambah Data</button>
                        </div> 
                    </form>
                </div> <!-- / .card -->
            </div> <!-- .col-12 -->
        </div> <!-- .row -->
    </div> 
</div>

<script>
    const nama = document.querySelector('#nama');
    const slug = document.querySelector('#slug');

    nama.addEventListener('change', function(){
        fetch('/dashboard/tambah-data-artikel/createSlug?nama=' + nama.slug())
        .then((response) => response.json())
        .then((data) => slug.value = data.slug);
    });
</script>

@endsection